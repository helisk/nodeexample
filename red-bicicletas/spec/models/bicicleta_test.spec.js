var Bicicleta = require('../../models/bicicleta');

//clean array bicis
beforeEach(() => { Bicicleta.allBicis = []; });

//test array equals 0
describe('Bicicleta.allBicis', ()=> {
	it('comienza vacía', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
	});
});

//add test bicicleta
describe('Bicicleta.add', () => {
	it('agregamos una', () => {
		var a = new Bicicleta(1, 'rojo', 'urbana', [14.560055, -89.337151]);
		Bicicleta.add(a);
		expect(Bicicleta.allBicis.length).toBe(1);
		expect(Bicicleta.allBicis[0]).toBe(a);
	});
});

// test findbyid
describe('Bicicleta.findById', () => {
	it('debe devolver la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);

		var aBici = new Bicicleta(1, 'verde', 'urbana');
		var aBici2 = new Bicicleta(2, 'verde', 'urbana');
		Bicicleta.add(aBici)
		Bicicleta.add(aBici2)

		var targetBici = Bicicleta.findById(1);
		expect(targetBici.id).toBe(1);
		expect(targetBici.color).toBe(aBici.color);
		expect(targetBici.modelo).toBe(aBici.modelo);
	});
});

// removeById test 
describe('Bicicleta.removeById', () => {
	it('debe eliminar la bici con id 1', () => {
		expect(Bicicleta.allBicis.length).toBe(0);
		
		var aBici = new Bicicleta(1, 'verde', 'urbana');
		Bicicleta.add(aBici)
		expect(Bicicleta.allBicis.length).toBe(1);
		
		Bicicleta.removeById(1);
		expect(Bicicleta.allBicis.length).toBe(0);

	})
})