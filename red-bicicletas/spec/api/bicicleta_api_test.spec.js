var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www')

describe('Bicicleta Api', () => {
	describe('GET Bicicletas /', () => {
		it('Status 200', () => {
			expect(Bicicleta.allBicis.length).toBe(0);
		
			var a = new Bicicleta(1, 'rojo', 'urbana', [14.560055, -89.337151]);
			Bicicleta.add(a);

			request.get('http://localhost:5000/api/', function(error, response, body){
				expect(response.statusCode).toBe(200);
			});
		});
	});

	describe('Post Bicicletas /create', () => {
		it('Status 200', (done) => {
			var headers = {'content-type' : 'application/json'};
			var aBici = '{"id" : 10, "color" : "rojo", "modelo" : "urbana", "lat" : 14, "lng": -89}';
			request.post({
				headers : headers,
				url : 'http://localhost:5000/api/create',
				body : aBici
			}, function(error, response, body){
				expect(response.statusCode).toBe(200);
				expect(Bicicleta.findById(10).color).toBe("rojo");
				done();
			});
		});
	});

	describe('Delete Bicicletas /delete', () => {
		it('Status 204', (done) => {
			var headers = {'content-type' : 'application/json'};
			var a = new Bicicleta(1, 'rojo', 'urbana', [14.560055, -89.337151]);
			Bicicleta.add(a);
			biciId  = Bicicleta.findById(1).id;

			request.post({
				headers : headers,
				url : 'http://localhost:5000/api/delete',
				body : biciId
			}, function(error, response, body){
				expect(response.statusCode).toBe(204);
				// expect(Bicicleta.findById(1).color).toBe("rojo");
				done();
			});
		});
	});

});